#!/bin/bash

cd $HOME
sudo apt update
sudo apt full-upgrade -y
sudo apt install -y git build-essential gdb gdbserver openssh-server g++ zip openssh-client g++-8 grep coreutils expect

echo 'eval $(ssh-agent -s)' >> .profile
echo "ssh-add $HOME/.ssh/id_ed25519" >> .profile

echo "#!/bin/bash" > update
echo set -x >>update
echo sudo apt update >>update
echo sudo apt -y full-upgrade >>update
echo sudo apt -y autoremove >>update
echo sudo apt -y autoclean >>update
chmod 777 update
./update

echo "#!/bin/bash" > push
echo set -x >> push
echo git add . >> push
echo git 'commit -m"Update"' >> push
echo git push >> push
chmod 777 push

read -ep 'Enter your name: ' name
read -ep 'Enter your email: ' email

git config --global user.name "$name"
git config --global user.email "$email"

ssh-keygen -o -f ~/.ssh/id_ed25519 -t ed25519 -C "$email"

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 10
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 20

sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 20

sudo update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 30
sudo update-alternatives --set cc /usr/bin/gcc

sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 30
sudo update-alternatives --set c++ /usr/bin/g++

echo -e "\e[1;33mGo to https://gitlab.com/profile/keys and enter \"\e[1;36m$(cat ~/.ssh/id_ed25519.pub)\e[1;33m\" in the key box.\e[0m"
read -p "Press [Enter] after entering your ssh-key."
