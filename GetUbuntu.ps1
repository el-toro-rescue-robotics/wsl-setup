New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock" -Name "AllowAllTrustedApps" -Value 1 -PropertyType DWORD -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock" -Name "AllowDevelopmentWithoutDevLicense" -Value 0 -PropertyType DWORD -Force

if ((Get-WindowsOptionalFeature -FeatureName Microsoft-Windows-Subsystem-Linux -Online).State -ne "Enabled") {
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux -NoRestart
}

if (!((Get-AppxPackage -Name *Ubuntu*).Name -match "[\s\S]*CanonicalGroupLimited\.Ubuntu18\.04onWindows[\s\S]*")) {
    (New-Object System.Net.WebClient).DownloadFile("http://aka.ms/wsl-ubuntu-1804", "$ENV:TMP\Ubuntu.Appx")
    Add-AppxPackage $ENV:TMP\Ubuntu.Appx
    Remove-Item -Path $ENV:TMP\Ubuntu.Appx
}

New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce" -Name "InstallUbuntu" -Value ($(Get-AppxPackage -Name  CanonicalGroupLimited.Ubuntu18.04onWindows).installlocation + "\ubuntu1804.exe")

Write-Output "Going to restart"
Pause
Restart-Computer -Force
