sudo apt install python python-pip git grep
sudo pip2 install future

sudo git clone --depth 1 https://github.com/raspberrypi/tools.git /opt/tools
exportline="export PATH=\$PATH:/opt/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin"
grep -Fxq "$exportline" ~/.profile 2>/dev/null || {
    echo $exportline >> ~/.profile
    . ~/.profile
}

cd $HOME
git clone https://gitlab.com/el-toro-rescue-robotics/ardupilot --depth 1
cd ardupilot
git submodule update --init --recursive --depth 1
