# WSL Setup
Setup the Windows Subsystem for Linux with Ubuntu.

These instructions only apply to Windows 10.  If you don't have Windows 10, you can't use the WSL or run Ubuntu on Windows.

To install the WSL, you need 64-bit Windows 10, and updated to at least Windows 10 version 1607 

To start, download or clone this repository.  Select and then right click on GetUbuntu.cmd and select 'Run as administrator'.  Wait for the script to finish.  Your computer will restart.

After restarting, Ubuntu should launch.  Follow the installation instructions to select a username and password.  These don't have to be the same as your windows ones.  After installing, type
`
"/mnt/c/path/to/ubuntu-setup.sh"
`
and click enter.  When the script finishes, your Ubuntu should be setup to compile for Linux machines.

To launch Ubuntu, select 'Ubuntu 18.04 LTS' in the start menu.
